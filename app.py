from flask_wtf import FlaskForm
from wtforms import StringField, FileField
from wtforms.validators import DataRequired

from flask import Flask, request, redirect, url_for, jsonify, render_template, send_file

from creational_design_patterns.builder_method.builder import construct_course, Complexcourse
from creational_design_patterns.factory_method.factory import Factory
from creational_design_patterns.abstract_factory_method.abstract_factory import Course, random_course
from creational_design_patterns.prototype_method.prototype import Courses_At_GFG_Cache
from creational_design_patterns.singleton_method.singleton import Singleton
from structural_design_patterns.adapter_method.adapter import MotorCycle, Adapter, Truck, Car

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


class MyForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    file = FileField()


@app.route('/factory_method_form/', methods=('GET', 'POST'))
def factory_method_form():
    if request.method == 'POST':
        lst = []
        for i in range(1, 4):
            lst.append(request.form[f'title{i}'])
        f = Factory("French")
        e = Factory("English")
        s = Factory("Spanish")
        return f'{f.localize(lst[0]), e.localize(lst[1]), s.localize(lst[2])}'
    return render_template('factory_method_form.html')


@app.route('/abstract_factory_method_form/', methods=('GET', 'POST'))
def abstract_factory_method_form():
    course = Course(random_course)
    return render_template('abstract_factory_method_form.html', course=course)


@app.route('/builder_method_form/', methods=('GET', 'POST'))
def builder_method_form():
    complex_course = construct_course(Complexcourse)

    return render_template('builder_method_form.html', complex_course=complex_course)


@app.route('/prototype_method_form/', methods=('GET', 'POST'))
def prototype_method_form():
    Courses_At_GFG_Cache.load()
    sde = Courses_At_GFG_Cache.get_course("1")
    dsa = Courses_At_GFG_Cache.get_course("2")
    stl = Courses_At_GFG_Cache.get_course("3")
    return render_template('prototype_method_form.html', Courses_At_GFG_Cache=Courses_At_GFG_Cache,
                           sde=sde, dsa=dsa, stl=stl)


@app.route('/singleton_method_form/', methods=('GET', 'POST'))
def singleton_method_form():
    s1 = Singleton()
    s2 = Singleton()
    s1_id = id(s1)
    s2_id = id(s2)
    return render_template('singleton_method_form.html', s1_id=s1_id, s2_id=s2_id)


@app.route('/adapter_method_form/', methods=('GET', 'POST'))
def adapter_method_form():
    objects = []
    motorCycle = MotorCycle()
    objects.append(Adapter(motorCycle, wheels=motorCycle.TwoWheeler))
    truck = Truck()
    objects.append(Adapter(truck, wheels=truck.EightWheeler))
    car = Car()
    objects.append(Adapter(car, wheels=car.FourWheeler))
    return render_template('adapter_method_form.html', objects=objects)