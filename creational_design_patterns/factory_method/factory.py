# Python Code for factory method
# it comes under the creational
# Design Pattern
from creational_design_patterns.factory_method.english_localizer import EnglishLocalizer
from creational_design_patterns.factory_method.french_localizer import FrenchLocalizer
from creational_design_patterns.factory_method.spanish_localizer import SpanishLocalizer


def Factory(language="English"):
    """Factory Method"""
    localizers = {
        "French": FrenchLocalizer,
        "English": EnglishLocalizer,
        "Spanish": SpanishLocalizer,
    }

    return localizers[language]()


# if __name__ == "__main__":
#
#     f = Factory("French")
#     e = Factory("English")
#     s = Factory("Spanish")
#
#     message = ["car", "bike", "cycle"]
#
#     for msg in message:
#         print(f.localize(msg))
#         print(e.localize(msg))
#         print(s.localize(msg))