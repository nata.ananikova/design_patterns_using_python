# Python Code for object
# oriented concepts using
# the abstract factory
# design pattern

import random

from creational_design_patterns.abstract_factory_method.data_structure_and_algorithms import DSA
from creational_design_patterns.abstract_factory_method.software_development_engineer import STL
from creational_design_patterns.abstract_factory_method.standart_template_library import SDE


class Course:
    """ GeeksforGeeks portal for courses """

    def __init__(self, courses_factory=None):
        """course factory is out abstract factory"""

        self.course_factory = courses_factory

    def show_course(self):
        """creates and shows courses using the abstract factory"""

        course = self.course_factory()

        return f'We have a course named {course}, its price is {course.Fee()}'


def random_course():
    """A random class for choosing the course"""

    return random.choice([SDE, STL, DSA])()


# if __name__ == "__main__":
#
#     course = Course(random_course)
#     for i in range(5):
#         course.show_course()
