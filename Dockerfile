FROM python:3.8-slim

COPY . /root

WORKDIR /root

RUN pip install flask gunicorn flask_wtf

#ENTRYPOINT flask run --host=0.0.0.0